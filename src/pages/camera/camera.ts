import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';

@Component({
  selector: 'page-camera',
  templateUrl: 'camera.html'
})
export class CameraPage {

  constructor(public navCtrl: NavController, private platform: Platform) {
	navigator.getUserMedia = navigator.getUserMedia ||
                         navigator.webkitGetUserMedia ||
                         navigator.mozGetUserMedia;
  }



  ngAfterViewInit() {
  		navigator.getUserMedia({ audio: false, video: { width: this.platform.width(), height: this.platform.height()* 0.8 } }, stream => {
  			var video = document.querySelector('video');
	         video.srcObject = stream;
	         video.onloadedmetadata = function(e) {
	           video.play();
	         };
  		}, console.log);
  }

  takeSnapshot() {
  	var canvas = document.querySelector('canvas');
	var context = canvas.getContext('2d');
	var video = document.querySelector('video');
	context.drawImage(video, 0, 0, 640, 480);
  }

  goBack() {
  	this.navCtrl.pop();
  }
}
